
<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/', function () {
  return view('paginaPrincipal.index');
});*/
//Route::get('/apisubastas','ApiSubastaController@index')->name('apisubastas');
Route::resource('/','subastaController');
Route::get('transparencia', function () {
    return view('art70.transparencia');
});
Route::get('transparencia07', function () {
    return view('art70.transparencia07');
});
Route::get('fiscal', function () {
    return view('fiscal');
});
Route::get('aplicabilidad', function () {
   return view('anexos.aplicabilidad');
});
Route::get('expedientesClasificados', function () {
   return view('anexos.expedientes');
});
Route::get('denuncia', function () {
   return view('anexos.denuncia');
});

Route::get('disciplinaFinanciera', function () {
    return view('diciplinaFinanciera');
});

Route::get('/index', 'subastaController@index');

Route::get('/empenos', function () {
   return view('paginaPrincipal.empenios');
});

Route::get('/articulos', function () {
   return view('paginaPrincipal.articulos');
});

Route::get('/ventajas', function () {
   return view('paginaPrincipal.ventajas');
});

Route::get('/pagos', function () {
   return view('paginaPrincipal.pagos');
});

Route::get('/sucursales', function () {
   return view('paginaPrincipal.sucursales');
});

Route::get('/boletas', function () {
   return view('paginaPrincipal.boletas');
});

Route::get('/tienda', function () {
   return view('paginaPrincipal.tienda');
});

Route::get('/subastas','subastaController@subastas');

Route::get('/directorio', function () {
   return view('paginaPrincipal.directorio');
});

Route::get('/historia', function () {
   return view('paginaPrincipal.historia');
});

Route::get('/codigoEtica', function () {
   return view('paginaPrincipal.codigoEtica');
});

Route::get('/valores', function () {
   return view('paginaPrincipal.valores');
});

Route::get('/contacto', function () {
   return view('paginaPrincipal.contacto');
});

Route::get('/preguntas', function () {
   return view('paginaPrincipal.preguntas');
});

Route::get('/ventajas', function () {
   return view('paginaPrincipal.ventajas');
});

Route::get('/terminos', function () {
   return view('paginaPrincipal.terminos');
});

Route::get('/nuevaTransparencia', function () {
   return view('paginaPrincipal.nuevaTransparencia');
});

Route::get('/sorteoMonte', function () {
   return view('paginaPrincipal.sorteoMonte');
})->name('sorteoMonte');

Route::post('estadoboleta','subastaController@estado_boleta');
