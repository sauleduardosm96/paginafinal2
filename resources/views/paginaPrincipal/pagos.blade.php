
@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')


       
       <div class="container type1">

       <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/pagos.jpg" /></div>
        </div>
       <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 100px; background:#b4106b;padding-top: 20px; color:#fff; ">
    <h2 class="text-center" style="color:#fff; ">Movimientos que puedes hacer en nuestras sucursales</h2>
    
  </div>
       </div>
         <div class="col-lg-6 pagos-info">   
    <h2>Refrendo:</h2>
    <p>Consiste  en pagar los intereses generados en el plazo. </p>
    <p>Estos sencillos pasos te ayudarán a refrendar en sucursal:</p><br><br>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0007_Layer-1.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Trae tu boleta a la sucursal donde empeñaste.</div>
    </div>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0001_Layer-2.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Acércate a ventanilla para preguntar el estado actual de tu prenda</div>
    </div>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0000_Layer-3.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Realiza el pago de los intereses generados desde tu último movimiento.</div>
    </div><div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0006_Layer-4.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Recibe una nueva boleta</div>
    </div>
    
  </div><div class="col-lg-6 pagos-info">   
    <h2>Abono:</h2>
    <p>Pago que se aplica al monto de tus prestamo e intereses generados en el periodo.</p>
    <p>Estos sencillos pasos te ayudarán a abonar en tu sucursal:</p>
    <br><br>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0007_Layer-1.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Trae tu boleta a la sucursal donde empeñaste.</div>
    </div>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0001_Layer-2.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Acércate a ventanilla para preguntar el estado actual de tu prenda</div>
    </div>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0000_Layer-3.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Realiza el pago de los intereses generados y abono a capital.</div>
    </div><div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0006_Layer-4.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Recibe una nueva boleta</div>
    </div>
    
  </div>
<div class="col-lg-6 pagos-info"> 
                     <hr>
  
    <h2>Demasía:</h2>
    <p>Dinero que pudieras recibir de la venta de tu prenda menos intereses y comisiones generadas en el plazo.</p>
    <p>Estos sencillos pasos te ayudarán a cobrar tu demasía:</p><br><br>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0007_Layer-1.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Trae tu boleta a la sucursal donde empeñaste.</div>
    </div> <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0003_Layer-7.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Trae tu identificación oficial y una copia.</div>
    </div> <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0001_Layer-2.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Acércate a ventanilla para preguntar el estado actual de tu prenda</div>
    </div><div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0004_Layer-6.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Recibe tu dinero</div>
    </div><div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0005_Layer-5.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Firma de recibido</div>
    </div>
</div>


          <div class="col-lg-6 pagos-info">   
                  <hr>
           <h2>Desempeño:</h2>
           <p>Recuperar tus prendas después de pagar el total de tu préstamo más los intereses que se generaron.           </p>
           <p>Con estos pasos Desempeña y recupera tus prendas:</p><br><br>
      
           <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0007_Layer-1.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Trae tu boleta a la sucursal donde empeñaste.</div>
    </div>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0001_Layer-2.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Acércate a ventanilla para preguntar el estado actual de tu prenda</div>
    </div>
    <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0000_Layer-3.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Realiza el pago total del importe mencionado en ventanilla.</div>
    </div>  <div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0008_Background.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Una vez hecho el pago, recibirás tu prenda.</div>
      
    </div><div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0002_Layer-8.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Revisa tu prenda.</div>
      
    </div><div class="row">
  <div class="col-lg-2"><img class="img-responsive pasos-im" src="img/movimientos/pasos_0005_Layer-5.png" alt=""/></div>
      <div class="col-lg-10 pagos-des">Firma de conformidad</div>
      
    </div>
			  
           <p><br>Para que te entreguen tu prenda debes acudir a la sucursal en la que empeñaste.</p>
  </div>
    
       </div>
@endsection

@include('plantilla.footerPrincipal')
    		</body>

</html>		