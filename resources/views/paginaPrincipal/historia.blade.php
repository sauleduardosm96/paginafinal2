@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')
<div class="container">

  <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/historia.jpg" /></div>
     </div>
  <div class="row">
   <div class=" col-lg-12"  style="height: 100px; background:#f5780f; padding-top: 2%; margin-bottom: 30px; text-align: center">
     <h2>Historia</h2></div>
  </div>
  
  
  
  
  
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
<!--
<div class="col-lg-8 col-lg-offset-2 historia">
      <h4>1. Casas de empeño</h4>
      <p>El primer antecedente de lo que sería el Monte de Piedad en Oaxaca, es cuando en 1849 son creadas las casas de empeño bajo la supervisión del gobierno; fundamentadas jurídicamente por medio de un decreto en un afán de normar y controlar la usura de los agiotistas clandestinos de la época, se cobra 6% de interés semestral tratando así de ayudar a la clase más necesitada. Estas casas de empeño son impulsadas por los liberales, siendo gobernador don Benito Juárez.</p>
      <p>Durante la administración de don Benito Juárez,2se nota una intensa actividad en todos los ámbitos; se reorganiza el poder judicial e impulsa la instrucción pública; se mandan levantar los planos de la ciudad y del estado de Oaxaca; se construye un hospital militar además de varios caminos; se inician los trabajos del puente sobre el río Atoyac, y se continúa la construcción del Palacio de Gobierno, donde siempre, a los servidores públicos se les paga puntualmente.</p>
      <p>En este decreto, el Congreso Constitucional del Estado Libre y Soberano de Oaxaca, establece casas de empeño y autoriza al gobierno para que las reglamente.</p>
      <p>Se permite el establecimiento de estas casas con un fondo de diez mil pesos como mínimo; con un interés de medio real por peso semestral, y el de un real ' por cualesquiera tiempo mayor de un año'.</p>
      <p>Pasados seis meses del plazo estipulado se remataban en subasta pública, con intervención de un alcalde, las prendas empeñadas, fijándose con anticipación de un mes los respectivos rotulones en las esquinas de las calles, dándose aviso en los periódicos; una vez hecha la venta se devolvía a los interesados el exceso del valor de sus alhajas.</p>
      <p>Se prevé que el gobierno, al formar los reglamentos necesarios, con audiencia de los interesados, cuide de fijar en ellos disposiciones 'para que los prestamistas no se nieguen a las solicitudes del público, con el pretexto de estar empleado todo el fondo'.</p>
      <p>Para esto disponía el decreto que: 'El que ponga un establecimiento del que habla el arto I o con la cantidad de diez mil pesos, garantizará la alhajas que reciba con una fianza o hipoteca de veinte mil pesos, a satisfacción del Ayuntamiento respectivo'.</p>
      <p>Lo tendrá entendido el Gobernador del Estado y compañía. Palacio de Gobierno del Estado de Oaxaca, septiembre 28 de 1849.3</p>
      <p>&nbsp;</p>
      <p>2 Del 11 de agosto al 11 de septiembre de 1846; del 29 de octubre de 1847 al 12 de agosto de 1852 y del 10 de enero de 1856 a1 25 de octubre de 1857.<br>
        3 Colección de Decretos del Gobierno del Estado de Oaxaca, tomo I, pp. 623 – 624.</p>
      
    </div>
      
    </div>
    <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia"><h4><br>
        2. Primer Monte de Piedad</h4>
      <p>El 30 de diciembre de 1868 el gobernador Félix Díaz, hermano de don Porfirio, funda el primer Monte de Piedad en la capital de Oaxaca, 'para alivio de las personas indigentes que tengan necesidad de ocurrir a préstamo con interés sobre prendas'.4</p>
      <p>Para el sostén del Monte Pío solicita al Congreso de la Unión autorización con el fin de establecer una lotería para sorteos semanarios de $250 pesos, y mensuales de $1 000 pesos.</p>
      <p>Además, que se utilicen del erario $10 000 pesos para el inicio de la institución, y se cobren intereses de 6% anual. Una vez logrado lo anterior se establece el primer Monte de Piedad en la casa N° 5 de la 2a. calle de Juárez (hoy Macedonia Alcalá) siendo al principio de mucha ayuda para el pueblo de Oaxaca por lo que se abrieron sucursales.</p>
      <p>Cuando es gobernador Félix Díaz,5entre sus aciertos, se cuenta el otorgar pensiones a los deudos de los defensores de la Independencia; el haber inaugurado el 19 de enero de 1868, con la colaboración de su hermano Porfirio, una línea telegráfica entre Tehuacán y Oaxaca;6Iniciar las obras de construcción de un camino entre Oaxaca y el istmo de Tehuantepec; además de establecer juzgados de primera instancia en los distritos de Oaxaca.</p>
      <p>En 1870, siendo aún gobernador el general de brigada Félix Díaz, se expide el reglamento que rige al Monte Pío y establece que contará con cinco empleados; un director con un sueldo anual de $1 400 pesos, un contador tenedor de libros, un valuador, un escribiente y un portero.7</p>
      <p>En este mismo año, la Legislatura aprueban las facultades concedidas al Ejecutivo por la ley del 28 de diciembre de 1868 para establecer y organizar al Monte de Piedad.</p>
      <p>Es así como establece una lotería 'para fomentar el Montepío', y lo faculta en su artículo 3° para que lleve a efecto el contrato de compra de la casa propiedad del C. Manuel Eduardo Goytia, tomando los fondos y arreglando los términos de él conforme fuera más útil al erario.8</p>
      <p>Este primer reglamento está fechado el 8 de abril de 1870, y lo firma aparte del gobernador, el secretario general del despacho Félix Romero.      </p>
      <p>4Decreto N° 53 <br>
        51° diciembre 1867 a 9 noviembre 1871.<br>
        6Enrique Ortiz, Breve Historia del Telégrafo en Oaxaca, Abarcando el tiempo comprendido de enero de 1868 a marzo 2 de 1928 por Enrique 0rtíz. Catedrático del ramo respectivo en el lnstituto de Ciencias Y Artes del Estado.<br>
        Talleres de Imprenta Y Encuadernación de Guillermo lturribarría. Oaxaca, México.<br>
        7 Colección de Leyes y Decretos, tomo IV pp. 638 - 639. Decreto N° 53. Firman Juan Ignacio Fagoaga. Diputado presidente, Félix Díaz gobernador y Francisco Rincón, secretario general del despacho. Oaxaca diciembre 30 de 1868.<br>
      8Colección, tomo V, pp. 250 – 251. Derecho N° 5. Oaxaca 4 de octubre de 1870 firman José Pardo diputado presidente, Félix Díaz gobernador y Félix Romero secretario general del despacho.</p>
      

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
      <h4>3. Compra de la Casa del Estanco</h4>
      <p>Días después,9el gobierno compra la propiedad de Manuel Eduardo Goytia y el 12 de octubre de ese mismo año se escritura ante el Escribano Público y de Número C. Juan Ocampo en su libro de protocolo.10</p>
      <p>En este libro se consigna la venta de la casa del ex Estanco (esq. Morelos y Alcalá) otorgada por el C. Roberto Maqueo como apoderado de Manuel Eduardo Goytia en favor del fisco del Estado para el Montepío y en que manifiesta Maqueo que: </p>
      <p>Habiendo convenido con el Superior Gobierno del Estado en venderle la casa conocida como del Estanco propiedad de su poderdante en la suma de seis mil pesos para el establecimiento del Monte de Piedad del Estado...<br>
        Que la finca objeto del contrato se ubica en la calle del Estanco Manzana once del Cuartel segundo de esta ciudad y se compone de treinta y una varas de ancho y cincuenta y seis de largo; circundándose entre las fincas rectas tiradas la del ancho de oriente a poniente tocando los límites por el norte de la casa conocida por la de Carbajal propiedad del exponente.<br>
        Y por el sur la calle de la Factoría 11 y los de largo que es de norte a sur, por el poniente toca los límites del ciudadano general Félix Díaz y por el oriente dicha calle del Estanco: que en consecuencia los linderos bajo las medidas expresadas, con el oriente calle de por medio, y casa enfrente de doña Rosario Carbajal; por el poniente dicha casa del citado general Félix Díaz, conocida como la de la Factoría; por el norte con la casa de la propiedad del que habla; y por el sur, calle de por medio y casa donde se halla el Instituto de Ciencias y Artes del Estado, que fue colegio seminario.</p>
      <p>Mientras esto sucede, Félix Díaz secunda a su hermano Porfirio en el Plan de la Noria, en contra de la permanencia en la presidencia de Benito Juárez, y por esto tiene que renunciar al cargo de gobernador el 9 de noviembre de 1871, y muere en forma brutal a manos de juchitecos el 22 de enero de 1872.</p>
      <p>Sustituye a Félix Díaz en el gobierno el regente de la Corte de justicia Félix Romero, quien por la lucha entre borlados y radicales,renuncia el 8 de enero de 1872 sustituyéndolo el General en Jefe del Ejército de Operaciones sobre Oaxaca Ignacio Alatorre quien ese mismo día le entrega el poder a Miguel Castro identificado con los borlados.</p>
      <p>Miguel Castro asume el poder, y se reelige el 18 de julio de 1872; día y año en que fallece el presidente Benito Juárez. Queda como Presidente de México Sebastián Lerdo de Tejada.</p>
      <p>Lerdo de Tejada no ve con simpatía al gobierno de Castro por apoyar al general Ignacio Mejía como posible sucesor a la presidencia de la República y que había sido gobernador de Oaxaca en 1852, y apoya a la fracción del Congreso del Estado que, a su vez, están a favor de José Esperón, Castro tiene que renunciar el 4 de noviembre de 1874, y es designado gobernador José Esperón.</p>
      <p>Ante esta situación de inestabilidad es cerrado el Monte de Piedad y deja de funcionar durante muchos años.</p>
      <p>Es hasta el 3 de enero de 1875, siendo gobernador de Oaxaca el licenciado José Esperón, cuando el Congreso l2 faculta al ejecutivo para que contrate un empréstito de veinte mil pesos para el establecimiento del Montepío, pudiendo hipotecar en garantía las rentas del Estado, situándose en la esquina de Murguía y 5 de Mayo en lo que había sido el convento de Santa Catalina, y que después sería el Palacio Municipal.13</p>
      <p>A la par de éste y el anterior Montepío, funciona alguna de las casas de empeño creadas durante la administración de Juárez.</p>
      <p>&nbsp;</p>
      <p>9De expedir el Decreto N° 5, de fecha 12 de abril de 1870.<br>
        10Archivo General de Notarías. Libro del año 1870. Escribano Juan Ocampo, pp. 476-478.<br>
        11Hoy Morelos.<br>
        12Decreto N° 21.<br>
        13Por alguna razón el edifico llamado del ex Estanco ya no lo ocupa el Montepío, y aunque el gobierno vuelve a vender años después el edificio a dicha institución, en la escritura no aparece ningún otro dueño.</p>
      
 

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
       
      <h4>4. Sucursal del Monte de Piedad de México</h4>
      <p>A las doce del día primero de diciembre de 1881 ante el Congreso del Estado de Oaxaca: protesta como gobernador el general Porfirio Díaz y dice en su discurso de toma de posesión:</p>
      <p>Con el fin de disminuir la pemiciosa influencia de la usura sobre las clases pobres y de evitar gran número de los sacrificios que se imponen a los desgraciados para saciar la avaricia de todos los que especulan con la miseria, he conseguido que el Nacional Monte de Piedad de México establezca en la capital del estado una sucursal dotada con los fondos que exige nuestra población al usar de tan benéfico establecimiento de préstamos. Los empleados de esta sucursal se encuentran en la capital y próximamente comenzarán a desempeñar sus funciones.14</p>
      <p>Esta sucursal queda establecida en la casa N° 36 de la avenida Hidalgo de esta ciudad y funciona por dos años.</p>
      <p>&nbsp;</p>
      <p>14Colección de Leyes, Decretos, Circulares y otras disposiciones dictadas por el Gobierno del Estado. Desde el 2 de diciembre de 1881 hasta el 31 del mismo mes de 1883. Tomo XI, Oaxaca, Imprenta del Estado en el Escuela de Artes y Oficios. A cargo De Ignacio Candían, 1887. Biblioteca del Archivo General del Estado de Oaxaca.</p>
      
 

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
      <h4>5. Reglamento de las casas de empeño</h4>
      <p>En ese mismo año de 1881 el presidente municipal de la ciudad de Oaxaca, José B. Camacho, elabora un reglamento para sujetar los procedimientos de las casas de empeño de la ciudad de Oaxaca, aprobándolo el Gobierno del Estado; en este reglamento se indica que para establecer una casa de empeño se requiere, 'obtener licencia del presidente municipal, la que se solicitará por escrito, acompañada la manifestación del capital en giro'.</p>
      <p>Además, señala que llevarán cuatro libros; uno en donde se lleven los asientos de las prendas; otro de los avalúos y remates; otro de contabilidad; el de las altas y bajas que diariamente tenga el capital, y el del talonario de las boletas de empeño; estos libros deben ser timbrados por la tesorería de rentas municipales.</p>
      <p>Se prohíbe recibir en las casas de empeño 'armas de munición que se conozca pertenecen a algún ramo del servicio público'. Y que para cerrar algún establecimiento se debe dar parte al Ayuntamiento.</p>
      <p>Asimismo, que los valuadores sean nombrados por el presidente municipal, y que la almoneda y remate se haga públicamente y al mejor postor. Por último, en un artículo transitorio recordaban a 'las personas que hubieran tenido licencia para establecer un giro en el ramo de empeños, se necesita refrendarla para continuar haciendo uso de ella conforme al reglamento'. Y que...</p>
      <p>a costa del tesorero municipal se imprimirán los ejemplares que a juicio del presidente municipal se consideren necesarios para uso de las casas de empeño para los valuadores, interventores, visitadores y demás funcionarios que tengan interés en su contenido. La tesorería municipal venderá a los que la soliciten los reglamentos impresos a 50 centavos el ejemplar.15</p>
      <p>Las continuas guerras intestinas e invasiones extranjeras que sufrió el país durante el siglo XIX se reflejaban en todos los ámbitos y hacían que esta institución de beneficencia pública no se consolidara.</p>
      <p>Después de estar cerrado el Monte de Piedad durante varios años, en un nuevo intento por abrirlo, el general de brigada Albino Zertuche,gobernador de Oaxaca, en 1889 es autorizado por el Congreso para que de los fondos numerarios aplique $50 000 pesos para este fin y que podría hacer llegar hasta la cantidad de $100 000 pesos para establecer en esta capital un Monte de Piedad.</p>
      <p>Depende del gobierno su administración y dirección y queda facultado el mismo gobierno para formar sus estatutos y los reglamentos de su instalación.</p>
      <p>Firman este decreto además de los diputados Presidente y Secretario G. Torres y F. Robleda, el Secretario General del Despacho Manuel Martínez Gracida y el gobernador Zertuche que muere poco tiempo después.</p>
      <p>&nbsp;</p>
      <p>15En agosto de 1884, siendo gobemador interino Mariana Jiménez, el Congreso de Oaxaca en su decreto N° 6 deroga y modifica dos artículos del Decreto del 8 de octubre de 1881.</p>
      
     
 

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
   
      <h4>6. Reapertura del Monte de Piedad</h4>
      <p>En 1891 cuando es gobernador don Gregorio N. Chávez, se instala un Monte de Piedad en el antiguo seminario de Oaxaca, en lo que hoy es el gimnasio de la UABJO por la avenida Morelos: es inaugurado el 5 de mayo de 1891.</p>
      <p>Para un mejor funcionamiento de la institución, se hacen reformas y adiciones al Reglamento del Monte de Piedad.16</p>
      <p>Y es perfeccionado por el gobernador Martín González en 1889.17</p>
      <p>Durante la administración del gobernador González, se establecieron la fábrica de cerveza La Mascota y la de cigarros La Ópera, que se anunciaba como 'La Gran Fábrica de cigarros, la primera en el Estado'; ubicada en el despacho 2a de la calle Benito Juárez N° 17; costaba la cajetilla diez centavos y estaba hecha con papel de china y una foto de alguna cantante de ópera de la época: la caja de 1000 cajetillas costaba $95 pesos. Además, se establecieron una fábrica de jabón y dos de sombreros de refinada manufactura.18</p>
      <p>Fue este gobemador duramente atacado por los periódicos El Estado, El Huarache, El Estandarte y El Vigilante, cuando se quiso reelegir, los columnistas Esteban Maqueo, Daría Pérez, José María Vidaña y Carlos Bravo lo acusaban de haberlo impuesto Porfirio Díaz. Martín González nació en Ocotlán en 1832 y participó como jefe del Estado Mayor en la mayoría de las campañas de don Porfirio.</p>
      <p>El gobernador Emilio Pimentel clausura la última casa de empeño, que había iniciado con Benito Juárez, pues del 6% semestral que empezaron cobrando habían subido hasta la usura, esta última casa de empeño estaba ubicada en el ex convento de Santa Catalina, frente al jardín Labastida, que después sería la cárcel.</p>
      <p>&nbsp;</p>
      <p>16En estas reformas, se especifican las condiciones de las boletas para préstamos menores de 25 centavos, hasta de $500 pesos; también se crea el reglamento económico determinando el objeto del Monte de Piedad, el tiempo de despacho, y su ejecución; de los empeños, desempeños, refrendos, colocación y arreglo de prendas; de las almonedas y de la contabilidad constando de 46 artículos y es firmado en abril de 1891.<br>
        17En su capítulo I norma el establecimiento y sus fondos; en el capitulo II habla de los depósitos; en el tercero del fondo de reserva; en otro, de las operaciones de empeño, desempeño y refrendo de prendas; de las almonedas, de las demasías; y en ese orden sigue diciendo de las prendas en comisión, del descuento de recibos de empleados públicos; del descuento de documentos comerciales; en el capítulo X enumera a los empleados, director con un sueldo de $4. I1 diarios para hacer un total de $1500 al año; los demás empleados son: un contador, un valuador, un depositario, un oficial de desempeño, un escribiente del contador, encargado de la Almoneda, un escribiente del valuador, un portero y un mozo. En su capitulo XI detalla las garantías que deben tener los empleados, y así sigue con las funciones y deberes de los empleados y de la contabilidad y documentación.<br>
        18Los gobernantes. pág.168.</p>
       
		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
      <h4>
        7. Construcción del Hospital General con las utilidades</h4>
      <p>Durante su gestión, el Congreso autoriza19que el Monte de Piedad del Estado continúe practicando sus operaciones con un capital efectivo de $50 000 pesos,20y lo que exceda de dicho capital y pertenezca al Monte, sea considerado como utilidades, y de ellas pueda disponer paulatinamente el gobierno, para la adquisición de un local y construcción de un edificio destinado al Monte de Piedad y a la compra de un terreno y construcción en él de un Hospital General, '... con las buenas condiciones que demandan en la actualidad los establecimientos de este género' .</p>
      <p>Una vez cubiertos los gastos de las obras realizadas, las utilidades del Monte se destinen a gastos de administración y fomento del referido hospital.</p>
      <p>También obtiene el Ejecutivo autorización para disponer de las sumas necesarias del capital o de las utilidades del Monte de Piedad para fundar en el estado varias sucursales de la institución, pues como se ve, tenía ganancias el Monte, no así la lotería creada para proveerlo de fondos en su inicio de funciones, así, nos encontramos que el gobierno tiene que financiar a la lotería para el pago de sus premios:</p>
      <p>...a fin de que la administración de la lotería La Protectora tenga a su disposición la cantidad que en numerario necesite para el pago de los billetes que salgan premiados en los dos sorteos que deberán verificarse el mes de mayo próximo, líbrese orden a la Tesorería general, para que, de los fondos de su responsabilidad, constituya en depósito en el Monte de Piedad del Estado, la cantidad de $8 000 pesos. Libertad y Constitución. Oaxaca de Juárez, abril 4 de 1904. - José Núñez.¬</p>
      <p>En medio de esta bonanza del Montepío en la capital de Oaxaca se abre una sucursal en Tlaxiaco el 3 de septiembre de 1904, 'que se considerará como una institución de beneficencia del Gobierno del Estado, dependiendo en su régimen interior de la casa matriz establecida en esta capital'.</p>
      <p>Un año después, en septiembre de 1905 al notar que son muchos gastos y ninguna utilidad, convienen en que la sucursal del Monte de Piedad de Tlaxiaco busque una mayor economía en los gastos de su sostenimiento y que 'para el movimiento que tiene la casa, no es necesario todo el personal que establece el decreto para su creación'.</p>
      <p>En 1902, Martín González quiso reelegirse, pero se encontró con la candidatura de Félix Díaz a quien su tío el presidente Porfirio Díaz y el partido de los intelectuales vetan e imponen a Emilio Pimentel como Gobernador de Oaxaca. El período de Pimentel está caracterizado por llevar a la práctica el anatema porfirista 'poca política y mucha administración', gobernó hasta el 4 de mayo de 1911. A él le toca la construcción e inauguración del Teatro Casino Luis Mier y Terán, hoy teatro Macedonia A/ca/á, del monumento a Juárez en el cerro del Fortín, y de los mercados Democracia, La Merced, Sánchez Pascuas y del Carmen entre otras construcciones. Inauguró asimismo las obras concluidas del Puerto de Salina Cruz, el ferrocarril de Tehuantepec y el ferrocarril entre Oaxaca y Ejutla, y Oaxaca y Tlacolula; además de fijar los límites entre Oaxaca y Veracruz.</p>
      <p>Esta prosperidad se reflejaba en la Institución de Piedad que veía sus utilidades crecer como lo indica el propio gobemador Emilio Pimentel el 6 de mayo de 1905, cuando manifiesta:</p>
      <p>Que en atención a que los productos de Monte de Piedad del Estado han tenido un aumento sensible en los últimos años, según lo comprueban los balances respectivos, y ese resultado en parte es de atribuirse a la laboriosidad de los empleados del establecimiento; sabemos que su trabajo irá en aumento ahora por la clausura de la mayor parte de las casas particulares de préstamos de esta capital.<br>
        El Gobierno ha acordado la creación de algunas sucursales del Montepío, que estará bajo la inmediata dirección de la Casa Matriz; la equidad exige aumentar también las dotaciones que tienen asignadas los empleados de referencia, así por las consideraciones anotadas como por el alza notable del precio en artículos de primera necesidad desde la fecha que les fueron asignados los sueldos que hoy disfrutan.</p>
      <p>Era el primer aumento en diez años y consistía, por ejemplo en $70 pesos de incremento al año al director del Monte de Piedad.</p>
      <p>En el año de 1911 el Monte de Piedad pasaba por un período de prosperidad, por eso, el gobernador interino del Estado, Heliodoro Díaz Quintas, ve que el trabajo en esta institución aumenta y se hace necesario 'dar mayor amplitud a la planta de empleados, por requerirlo el desarrollo de la misma'.También aumenta la planta de empleados de las sucursales y previene que 'los jefes de estos establecimientos caucionarán su manejo por la cantidad de $500.00 como establece el contrato que el Gobierno del Estado celebró el 26 de junio de 1903 con la sucursal de la America Surety Company de New York'.</p>
      <p>En medio de esta prosperidad falta un local a propósito para el Monte de Piedad pues se instaló provisionalmente (1911) en el local que hoy ocupa21y que es insuficiente para la debida separación de los departamentos que el reglamento exige, y para el depósito de los objetos empeñados.</p>
      <p>El gobierno considera que es necesario arrendar una casa particular para el almacén de las prendas que últimamente, en virtud del aumento de las operaciones y de la importancia que el establecimiento ha alcanzado, es indispensable instalarlo definitivamente en un local conveniente: que a ese fin el gobernador Heliodoro Díaz Quintas, tiene concertada la compra del sitio de la casa N° 7 de la 2a. calle de Trujano de esta ciudad, en donde podrá construirse un edificio que llene las condiciones necesarias al objeto a que se le destine. 'Ya que la ley del 16 de noviembre de 1903 lo faculta a... disponer lo necesario para cubrir los gastos de la mejora que trata de implementarse, y es necesario aplicar parte de dichos fondos al objeto indicado'.</p>
      <p>Se autoriza el gasto necesario para adquirir un local y construir en él un edificio destinado a casa matriz del Monte de Piedad y que de los fondos empleados se tomen de las utilidades de dicho establecimiento para que la construcción del edificio.</p>
      <p>Se aprueba el contrato que celebró la Tesorería General del Estado con el señor Amado H. Santibáñez, sobre compra para el edificio de la casa N° 7 de la 2a calle de Trujano de esta ciudad, en la suma de $5,250 pesos. Según la escritura pública de la compraventa del edificio del Monte de Piedad que hoy ocupa, este terreno es cedido por el Monte al gobierno como parte del pago de dicho edificio.</p>
      <p>&nbsp;</p>
      <p>19Del decreto N° I 2 de fecha I 6 de noviembre de 1903.<br>
        20Siendo, además, fondos de la casa los que señala el artículo 3° del reglamento de 22 de febrero de 1895. <br>
        21En el antiguo seminario de Oaxaca, hoy Facultad de Leyes de la UABJO.</p>
           
 

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
      <h4>
        8. Clausura de la lotería La Protectora</h4>
      <p>No todo eran buenas noticias para el gobernador Emilio Pimentel; el 31 de julio tiene que manifestar:</p>
      <p>Que habiendo demostrado la experiencia que los resultados de la lotería La Protectora no corresponden al objeto de su institución, que no fue otro que el de construir con sus productos un nuevo hospital general con las principales comodidades, servicios y adelantos de los modernos establecimientos de su género: que han sido inútiles los laudables esfuerzos hechos por su Junta Directiva y por el mismo Gobierno para acrecentar la venta de billetes, con la cual se habrían aumentado indudablemente sus productos: que de continuar más tiempo este estado de cosas, se perjudicarían los fondos públicos destinados a otros servicios de la administración.</p>
      <p>Es así como el 10 de septiembre de 1907 queda suprimida la lotería La Protectora creada por decreto de 1903. Igual que la lotería, la sucursal de Tlaxiaco no funcionaba como esperaban; por eso, el 28 de agosto del mismo 1907 decreta que se suspenden las operaciones de préstamo sobre prendas y refrendos de las mismas.</p>
 

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        <h4>9. La Revolución</h4>
      <p>Al conocerse la renuncia del presidente Porfirio Díaz en Oaxaca, el gobernador Emilio Pimentel renuncia y los revolucionarios se pronuncian porque el gobierno interino recaiga en uno de sus partidarios que sería Heliodoro Díaz Quintas.</p>
      <p>Aunque el Congreso del Estado no coincide con el movimiento revolucionario es nombrado Díaz Quintas por el período del 8 de junio al 23 de septiembre de 1911, en estos tres meses y medio de su administración mantiene a los revolucionarios en calma, crea escuelas de instrucción superior y normales para profesores. Después de Díaz Quintas, es elegido como gobernador Benito Juárez Maza.</p>
      <p>El 22 de abril de 1912 muere el gobernador de Oaxaca Benito Juárez Maza, hijo del Benemérito, la Cámara de Diputados nombra gobernador interino al diputado Alberto Montiel.</p>
      <p>En Oaxaca el capitán Pedro León se levanta con el Batallón Sierra Juárez, y se remonta a la sierra rumbo a Ixtlán, tomando la cabecera de Ixtlán; y sintiéndose fuerte trata de tomar la capital de Oaxaca pero es rechazado por el batallón Voluntarios de Oaxaca.</p>
      <p>A Montiel lo sucede Miguel Martínez el 22 de julio de 1912, y el 19 de agosto de ese año le entrega el poder a Bolaños Cacho.</p>
      <p>Durante la administración de Bolaños Cacho los ixtepejanos intentan tomar la ciudad de Oaxaca y saquean la villa de Etla; se calman cuando Victoriano Huerta en contubernio con la embajada de Estados Unidos, en febrero de 1913, dan cuartelazo y asesinan a Madero y Pino Suárez.</p>
      <p>El usurpador Huerta se nombra presidente y es reconocido por Bolaños Cacho por lo que crece la inconformidad en Oaxaca. En marzo Carranza desconoce a Huerta. El 14 de julio renuncia Bolaños Cacho. Huerta renuncia poco después.</p>
      <p>Para comprender el impacto que tuvo la Revolución y la caída de Porfirio Díaz en Oaxaca, sirva de ejemplo la suerte que corre el Monte de Piedad, que de ser una institución fuerte y solvente al grado de sacar fondos suficientes para la construcción del Hospital Civil, tiene que cerrar sus puertas durante varios años.</p>
      <p>La inercia de la buena administración de esta institución hace que se mantenga funcionando después de la caída de don Porfirio Díaz, hasta que se colapsa en 1916 producto de la guerra intestina.</p>
      <p>Haciendo un recuento de los hechos de armas más relevantes durante la Revolución, nos remontamos al 25 de marzo de 1911, cuando más de doscientos revolucionarios de Guerrero y Puebla toman por asalto las poblaciones de Oaxaca de Santa Ana Rayón y La Cieneguilla, al mando de Magdalena Herrera.</p>
      <p>El Gobierno del Estado al enterarse de esta noticia por el jefe político de aquella región, ordena a los destacamentos de Tlaxiaco, Huajuapan y Teposcolula que los persigan.</p>
      <p>Es apoyado el gobierno de Oaxaca por los gobiernos de Guerrero y Puebla, quienes ordenan a los jefes políticos de Chiautla, Huaxmuxtitlán, Tlapa y Acatlán que se unan a los de Silacayoapan.</p>
      <p>Después de ser dispersos los revolucionarios el 16 de abril del mismo año de 1911 se presentan nuevamente los maderistas: por Nicolás Hidalgo con 300 hombres entra Antonio Machaca; por Santa Cruz, Antonio Ruiz con otros 300 hombres y por Coycoyán, Gabriel Solís con 1000 hombres, todos con dirección a la cabecera de Tlaxiaco. El gobierno intenta mandar un destacamento pero los pueblos se habían vuelto maderistas y no tiene apoyo por lo que desiste de la idea.</p>
      <p>El 2 de mayo ocupan Tlaxiaco los revolucionarios del estado de Guerrero, al mando de Gabriel Solís, Luis Antonio Muchaca y Pedro Peña, dándose lectura al Plan de San Luis. Cambiaron a las autoridades y marcharon para la capital del Estado.</p>
      <p>El 20 de mayo de 1911, las fuerzas maderistas procedentes de Putla toman Tlaxiaco, se les reúnen Febronio Gómez y Ramón Cruz que venían de Jamiltepec.</p>
      <p>Gabriel Solís en su paso por Huajuapan, Teposcolula y Yanhuitlán en combinación de las fuerzas de Tlaxiaco.</p>
      <p>El 21 de mayo llegaron a Nochixtlán, el 5 de junio pasaron por Huitzo y el 16 de junio llegaron a Etla donde permanecieron.</p>
      <p>Este ejército liberador como se hacía llamar, deseaba entrar triunfante a Oaxaca sin haber tenido combate.</p>
      <p>El señor Guillermo Meixueiro fue comisionado por el Gobernador del Estado para conferenciar con los rebeldes con el fin de que no avanzaran hasta la capital.</p>
      <p>Al renunciar don Porfirio Díaz (mayo de 1911) gobernaba en Oaxaca su sobrino, el general Félix Díaz quien presionado por los maderistas, renuncia el 5 de junio de 1911.</p>
      <p>Nombra el Congreso del Estado al Lic. Jesús Acevedo como gobernador quien renuncia también el 8 de junio de 1911.</p>
      <p>Se elige a Fidencio Hernández quien de la misma manera renuncia el 8 de julio.</p>
      <p>Es nombrado después el Lic. Heliodoro Díaz que gobierna hasta que haya nuevo gobernador constitucional conforme al Plan de San Luis.</p>
      <p>Surge la candidatura del señor Benito Juárez Maza en 1911 junto con la del general Félix Díaz, ganando el primero y entrando a gobernar Oaxaca el 23 de septiembre del mismo año. Apoyado por Madero y con fuerte crítica del pueblo, prensa local y nacional.</p>
      <p>Se decía que no se había acatado a la Constitución por no reunir los requisitos de residencia y vecindad, además de no respetar el sufragio efectivo y la no-reelección.</p>
      <p>Murió el señor Juárez Maza de una afección cardiaca en 1912 a las 11 de la noche.Inmediatamente se reunió el Congreso para elegir gobernador interino.</p>
      <p>Fue electo gobernador Alberto Montiel, quien rige del 22 de abril al 23 de julio de 1912.</p>
      <p>Durante su breve interinato la Revolución se incrementó, según algunos autores por medidas equivocadas del gobierno para sofocarla y por las imposiciones que llevó a cabo en las elecciones de Diputados al Congreso de la unión.</p>
      <p>El batallón de serranos creado por el gobernador Juárez Maza deserta remontándose a la Sierra Juárez donde levantan a algunos pueblos y tratan de tomar la capital de Oaxaca el 27 de mayo de 1912.</p>
      <p>Las autoridades mandan una columna del Primer Cuerpo de Rurales hacia San Felipe del Agua encontrándose con cerca de dos mil serranos en las lomas de San Luis, empezando la batalla a mediodía, avanzando el combate de la hacienda de Guadalupe hasta Aguilera.</p>
      <p>Con refuerzos de 150 voluntarios de los Auxiliares de Oaxaca. Ayudan a la caballería en el ataque por los flancos que le infligen a los serranos, dominando las alturas del Crestón y Loma Pelada haciendo huir a los serranos, quedando muchos muertos en los cerros.</p>
      <p>El 11 de septiembre regresan a Las Huertas los rebeldes serranos mandando pedir la plaza, al no obtener respuesta positiva se dirigen al cerro del Crestón y Loma Pelada, otros toman posiciones detrás del acueducto y los demás se dirigen a San Luis.</p>
      <p>Se encuentran con el Batallón 48 de Voluntarios de Oaxaca; Auxiliares de Guerrero (formado por oaxaqueños) el 39 de Jalapa, 4 piezas de artillería ligera y 2 de tiro rápido, varios escuadrones de rurales, el 12 Regimiento y piquetes de la Guardia Nacional.</p>
      <p>El combate empezó a mediodía cuando ascendían los rebeldes por el Crestón, la artillería ligera rompió fuego protegiendo el avance de la infantería que les salía al encuentro.</p>
      <p>Un cañonazo al Crestón hizo que descendieran replegándose en tiradores y haciendo frente a nuestra infantería que avanzaba sobre ellos, otros se replegaron a la cañada, de donde los desalojaron dos cañonazos del Fortín. Nuestras tropas ganaban terreno a pesar de la superioridad numérica y armamento de los asaltantes, reconcentrándolos en San Felipe.</p>
      <p>La artillería avanzó también y desde las lomas de San Felipe elevó sus proyectiles hasta la cima causando muchos muertos y heridos a los serranos.</p>
      <p>Los rebeldes se refugiaron en Huayapam durante 10 días (del 13 al 23 de septiembre) el 24 la Zona Militar dispuso un plan de ataque sobre Huayapam atacando en la madrugada por varios puntos.</p>
      <p>Causó mucha pena como desde la ciudad de Oaxaca se podía ver como ardía Huayapam y durante el día recorrían las calles afligidas madres lamentando la muerte de sus deudos.22</p>
      <p>&nbsp;</p>
      <p>22Cayetano Esteva, Nociones elementales de Geografía Histórica del Estado de Oaxaca, San Germán Hnos., 1913.</p>
      <p>&nbsp;</p>
      
 

		</div>
    </div>    
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
  <h4>10. Segunda compra de la casa del ex Estanco</h4>
      <p>En medio de todos estos acontecimientos, el 3 de marzo de 1913, el director del Monte de Piedad Julio Fenelón, gestiona ante el gobernador de Oaxaca Miguel Bolaños Cacho, la compra de un inmueble para establecer a esta Institución ya que:</p>
      <p>... es materialmente imposible que ese establecimiento continúe en el pequeño e incómodo edificio que actualmente ocupa y que su existencia y departamentos continúen divididos en diversas casas como sucede por falta de amplitud del expresado local.<br>
        Que tanto el ingeniero del Estado, como el Tesorero General, han informado sobre el avalúo y precio del edificio que ocupa la Escuela Normal para Profesores, ubicado en la esquina de la 2ª. de Benito Juárez, 23 y calle 6ª. Avenida Morelos y con arreglo a esos datos y a la opinión del Ejecutivo...<br>
        Considera que este edificio es un local apropiado, amplio y perfectamente situado para la instalación y labores del citado Monte de Piedad.<br>
        Que aunque... el primero de septiembre de 1911 se autorizó el contrato de compra del terreno en que estuvo ubicada la casa número 7 de la calle Trujano y esa compra se llevó a efecto en la suma de $ 5 250 pesos... y se autorizó la construcción del Edificio del referido Monte de Piedad en el indicado terreno, este no tiene, ni la extensión necesaria, ni la ubicación conveniente, por lo que elige otro edificio, sin perjuicio de que el terreno indicado y la parte comenzada a construir en él, sean destinados por el mismo Monte de Piedad, o que si el gobierno más tarde lo quiere utilizar para la construcción de un edificio escolar moderno, reintegrando su valor a dicha Institución de Beneficencia Pública, sujeto a sus normas y con su contabilidad particular en que debe figurar como propio.24</p>
      <p>El Monte de Piedad, instalado en 1891 en la calle de Morelos, gozaba de gran prestigio y concurrían casi todas las clases sociales.</p>
      <p>Es así como el local empieza a ser pequeño para las necesidades de la institución, y aunque se abrieron sucursales por la Merced, el director don Pedro Vasseur plantea la necesidad de adquirir un local apropiado.</p>
      <p>Como hemos visto, es hasta que es gobernador Miguel Bolaños Cacho y director del Montepío Julio Fenelón que se decide por adquirir un local más amplio para el funcionamiento del Monte de Piedad.</p>
      <p>En ese entonces funcionaba en el edificio escogido para establecer el Monte de Piedad la Escuela Normal para Profesores del Estado de Oaxaca.</p>
      <p>Esta casa está ubicada en la 2ª. calle de Macedonio Alcalá, antes calle del Estanco, en el N°. 5 (hoy edificio del Monte de Piedad) originalmente ahí durante la colonia estuvo el estanco del tabaco, en 1858 fue Casa de Moneda, después Monte de Piedad, más tarde residió allí la Corte de Justicia, posteriormente la escuela Normal para Profesores, en 1913 fue derruida en gran parte para instalar de nuevo el Monte de Piedad, en 1928 fue residencia de los poderes del Estado,25y durante la mayor parte del siglo xx estuvo en los altos el Tribunal Superior de Justicia y en los bajos el Monte de Piedad y la Imprenta del Estado.</p>
      <p>Sustituye como administrador del Monte a don Julio Fenelón, Demetrio Calvo; y en el año de 1916, el 2 de marzo, las fuerzas soberanas evacuan la ciudad y fuerzas extrañas a Oaxaca entran y saquean varios comercios y el Monte de Piedad; la gente desconcertada e impotente, veía como se perdían sus pertenencias, sobre todo sus joyas y armas.</p>
      <p>Hubo algún intento del nuevo gobierno porque volviera a funcionar, designando director a Agustín Mústieles, pero fue un fracaso y se cerró definitivamente en 1916.</p>
      <p>&nbsp;</p>
      <p>23 La calle del Estanco al principio de la colonia fue conocida como de Santo Domingo, después del Estanco, luego Benito Juárez y hoy es conocida como Macedonio Alcalá.<br>
        24Archivo del Registro Público de la Propiedad. Notario Público Lic. Juan Varela, junio 5 de 1913.<br>
        25Ángel Taracena, Efemérides Oaxaqueñas, Oaxaca, 1941.</p>

		</div>
    </div>    
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
       <h4>11. Saqueo y cierre del Monte de Piedad</h4>
      <p>El saqueo y cierre del Monte de Piedad sucede durante la administración de José Inés Dávila, quien entra como gobernador el 6 de diciembre de 1914.26</p>
      <p>A principio de 1915, existe hambruna en el estado, y en agosto del mismo año se desata una epidemia de tifo, además, gran parte del erario se destina para oponer resistencia a los carrancistas que quieren incorporar a la guerra de la Revolución a Oaxaca y tienen tomadas la Costa, el Istmo y Tuxtepec. El 3 de junio de 1915, el gobernador Dávila y el Congreso de Oaxaca se pronuncian por la soberanía y emiten un decreto donde asientan que Oaxaca se separa del pacto federal.</p>
      <p>Ante esto, Carranza, en agosto de 1915, nombra al general Jesús Agustín Castro comandante militar y gobernador de Oaxaca, iniciando una campaña militar contra la soberanía de Oaxaca. Castro establece su gobierno en Salina Cruz, quien por medio de un decreto27 nombra fuera de la ley al gobernador José Inés Dávila; éste se sostiene seis meses como gobernador en Oaxaca en medio de fuertes batallas entre soberanistas y carrancistas hasta que el 2 de marzo de 1916 son derrotados los soberanistas en el combate de Ocotlán. Al día siguiente los soberanistas evacuan la ciudad junto con los zapatistas que les habían brindado ayuda; y son estas tropas zapatistas de Higinio Aguilar las que saquean la ciudad incendiando la sección del palacio donde estaban los archivos de los tribunales tratando de quemar expedientes: y la estación del Ferrocarril Mexicano del Sur, donde, para saquear una bodega de azúcar, se ponen a horadar una de las paredes, sin reparar en la melaza que escurría de la bodega; y cuando por fin cedió la pared de cantera, el fuego que había adentro respiró y salió una inmensa llamarada; además de saquear comercios y el Monte de Piedad, de donde extraen principalmente las joyas y armas que ahí se encontraban. Constantino Chapital (padre de quien fuera gobernador del mismo nombre) organiza a la gente para apagar el fuego del Palacio de Gobierno, y meten los objetos y documentos en lo que hoy se conoce como El Importador.</p>
      <p>Con una comisión se dirige a Tlacolula a pedir a los carrancistas que entraran a la ciudad para evitar los saqueos pues se encontraba desguarnecida la ciudad.</p>
      <p>Las fuerzas del general Castro constan de 10000 hombres con las brigadas Plan de Guadalupe, Usumancita y la división 21 comandadas por los generales Juan Jiménez Méndez, Macario (conocido como Matario) M. Hernández y Juan José Baños respectivamente, así como el Teniente Coronel Alberto de la Fuente.</p>
      <p>&nbsp;</p>
      <p>26R. Fortson, Los Gobernantes de Oaxaca. Historia 1823 – 1985, México D.F., J. R. Fortson, 1985.<br>
        27Periódico Oficial, alcance al Núm. 9, tomo II, Salina Cruz Oaxaca, marzo 8 de 1916.</p>
      <p>&nbsp;</p>  
 

		</div>
    </div>    
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        <h4>12. Reapertura</h4>
      <p>Diecisiete años después volverían a abrir nuevamente esta institución, siendo gobernador del estado el Lic. Anastasio García Toledo, con el fin de detener la voracidad de los agiotistas que exprimían a los que necesitaban de un crédito y no lo podían conseguir por medio de los bancos.</p>
      <p>Es así como presenta la iniciativa de ley para la creación de esta institución; dicha iniciativa es aceptada por el Congreso el 11 de enero de 1933.</p>
      <p>Es inaugurado el Monte de Piedad en la casa N° 5 de la calle de Armenta y López por estar ocupado su edificio por el gobierno del estado, pues el Palacio de Gobierno había quedado seriamente dañado por los temblores de 1928 y 1931.</p>
      <p>El gobernador Toledo se da a la tarea de reparar el Palacio de Gobierno, y el edificio del Monte es desocupado por el ejecutivo y vuelto a ocupar por el Monte de Piedad, que regresa a su edificio de Morelos y Alcalá en 1936.</p>
      <p>Para el establecimiento de esta benéfica institución, el Congreso del Estado de Oaxaca decreta el 11 de febrero de 1933, autorizando al ejecutivo del Estado para que de los fondos existentes en la Tesorería General pueda aplicar la cantidad de $50 000 pesos.</p>
      <p>Además, declaran prescritos todos los derechos y aplicaciones que puedan alegarse y pretendan hacerse valer en conexión con el funcionamiento del extinto Monte de Piedad del Estado.</p>
      <p>En el periódico El Oaxaqueño28(antes Mercurio) del 10 de marzo de 1933, entre anuncios del cómico Delgadillo en el teatro Terán ,29 de la pasta dentífrica Rebeca, la tienda Al Modelo en el Portal de Mercaderes N° 4 en que vendían cachemires, sombreros (y era, además, armería), anuncios de los cigarros Monte Carlo y de la matinée en el teatro Juárez, 30 se da la noticia en primera página de</p>
      <p>'la Solemne Inauguración del Monte de Piedad del Estado se llevó a cabo ayer' y que la declaratoria había sido hecha por el gobernador Anastasio García Toledo que dice:</p>
      <p>Ayer a las once horas se efectuó la inauguración del Monte de Piedad del Estado, con toda solemnidad, en el local que ocupa esta institución de crédito sita en la casa número 5 de Armenta y López de esta ciudad.</p>
      <p>Desde hora anticipada la banda de policía del Estado estuvo ejecutando piezas de su repertorio frente al edificio de la Institución, y, a la hora indicada, entonó el Himno Nacional Mexicano anunciando la presencia del primer Magistrado del Estado, licenciado Anastacio García Toledo.</p>
      <p>Una vez en el recinto, el señor Gobernador Constitucional del Estado tomó la palabra para hacer la declaratoria de la apertura del Monte Pío: haciendo antes un ligero preámbulo ante la concurrencia que asistió al acto inaugural, la cual estaba integrada por las siguientes personas: licenciado Aristeo V Guzmán, sub Secretario General del Gobierno; general Federico R. Berlanga, jefe de Operaciones Militares; Guillermo A. Esteva, Secretario Particular del Gobernador; Alejandro Rueda Camacho, jefe de la Oficina Federal de Hacienda; licenciado Benigno V Jiménez, procurador General de justicia del Estado; diputados licenciado Policarpo Sánchez, Emilio Díaz Ortiz y Rubén Rojas Espejo: regidor Alfredo Canseco Feraud, Sra. Harloe Hamilton, Gabriel Y. Carsolio, gerente del Banco Nacional de México, sucursal Oaxaca.</p>
      <p>Se inicia con un capital de $50 000 pesos y operaciones de pequeña cuantía, pero a medida que se requiera se aumentará la importancia de las operaciones.</p>
      <p>Como encargado de la gerencia queda Alejandro Marín; el encargado de la administración es el contador Lorenzo Barroso Gómez; valuador, Senorino Gutiérrez; depositario Hermenegildo Luis y escribiente, Amado E. Zurita.</p>
      <p>En El Oaxaqueño del 10 de marzo de 1936, cuando se cumplen tres años de la inauguración del Monte, y con motivo de que regresa a su edificio, sale una crónica que dice:</p>
      <p>El día de ayer 9 de marzo cumplió el Monte de Piedad del Estado tres años de vida activa normal, correspondiente a su segunda época, pues su reapertura se debe a un decreto del actual gobiemo en marzo de 1933, en virtud de que representa para el público menesteroso un factor de equilibrio económico de carácter doméstico. El Monte de Piedad había dejado de funcionar antaño, desde la época del carrancismo; veinte años estuvo cerrado con la consiguiente pérdida de su capital y los objetos de su almacén. Hubo necesidad de crear nuevamente la Institución y dotarla de un fondo suficiente para el desarrollo de sus operaciones.</p>
      <p>&nbsp;</p>
      <p>28El Oaxaqueño. Diario de Información. Viernes, 10 de marzo de 1933. Año XIII. Núm. 5278. A.G.E.O. <br>
        29Hoy teatro Macedonia Alcalá.<br>
        30Se ubicaba en donde hoy está la Escuela de Comercio a un lado del jardín Labastida.</p>
 

		</div>
    </div>    
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
 <h4>13. Regreso a su edificio</h4>
      <p>Tres años después de ser reabierto el Monte de Piedad regresa a su edificio.31</p>
      <p>El Gobierno del Estado que encabeza el señor licenciado García Toledo, por conducto de su secretaría general, ha demostrado su satisfacción por el buen camino que lleva el Monte. Desde hace tres años imparte sus beneficios a los hogares de la gente pobre, generalmente la trabajadora, y su curso ascendente hace esperar en el futuro un mejor rendimiento de auxilios.</p>
      <p>Hemos podido tomar apuntes de un informe general que rindió el director de la institución, señor Núñez, al gobierno; dice este documento que se efectuaron 148 602 operaciones de empeño con un préstamo total de $431 240.35; 124 845 operaciones de desempeño y refrendos por un valor de $363 964.43; 14,148 operaciones de almoneda por un valor de $30 410.73.</p>
      <p>Esta benéfica institución está servida por el siguiente personal. Director C. Miguel Ángel Nuñez; Sub Director C. Lorenzo Barroso G.; Valuador, C. Senorino Gutiérrez; Depositario, C. Amado Zurita; Escribiente C. Ángel Viloria; Auxiliar del Departamento, C: Enrique Santibañez; Desempeño, C. Fernando Felguérez.</p>
      <p>El 24 de noviembre de 1943 siendo gobernador el general de división Vicente González Fernández, y secretario general del Despacho el Lic. Raymundo Manzano Trovamala.</p>
      <p>Se crea el Reglamento General del Monte de Piedad del Estado de Oaxaca, por decreto de 11 de febrero de 1933, conteniendo 13 capítulos y 80 artículos; firman también este decreto el Diputado Presidente de la Legislatura de Oaxaca Guadalupe A. Bustamante y los diputados secretarios Ricardo López Gurrión y Manuel L. Bermúrdez.32</p>
      <p>&nbsp;</p>
      <p>31Según lo consigna el periódico El Oaxaqueño del 10 de marzo de 1936.<br>
        32Compilación de Ordenamientos Jurídicos de la Administración Pública Estatal. Tomo II mayo 1989. Reglamento General del Monte de Piedad. CILSEO. Archivo General.</p>
      <p>&nbsp;</p>

		</div>
    </div>
     <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
         <h4>14. Directores</h4>
      <p>Desde su reapertura en 1933 a la fecha han sido directores de esta Benéfica Institución:</p>
      <p>l. El señor Lorenzo Barroso Gómez.<br>
        2. El señor Miguel Ángel Núñez.<br>
        3. Don Manuel Gracida Carrizosa. 4. El señor José Álvarez Vasseur.<br>
        5. El señor Enrique Sumano.<br>
        6. Don Jesús Martínez Isaac.<br>
        7. Don Alfonso Gómez Zorrilla.<br>
        8. El señor Fausto Calvo Sumano.<br>
        9. Don Pedro Ramírez Cruz.<br>
        10. Cont. Sergio Canseco Vasconcelos.<br>
        11.Notario Público Lic. Moisés Ruiz Cruz.<br>
        12.Omar Ríos Flores.<br>
        13. Marcial Pérez Hernández.<br>
        14. Leonardo Pacheco Skidmore.<br>
        15. José Antonio Gutiérrez Bello.<br>
        16.Carlos Hampshire Franco.</p>
 

		</div>
    </div>
       <div class="item">
      <div class="col-lg-8 col-lg-offset-2 historia">
        
  <h4>15. El tesoro de Juárez</h4>
      <p>En el Monte de Piedad se han visto desfilar a miles de personas humildes; aunque también gente encumbrada acude a solicitar sus servicios, cada una con una historia que contar; referiremos una que por su singularidad vale la pena.</p>
      <p>En septiembre de 1940, en un periódico de la ciudad de México, 33 apareció un reportaje en que se relata que:</p>
      <p>Más de medio siglo de estancia en el Nacional Monte de Piedad tienen las joyas obsequiadas al Benemérito de América, don Benito Juárez, como premio a su labor patriótica; fueron llevadas por el hijo político del Benemérito, Manuel Dublán, las joyas quedaron en el olvido, hasta que fueron trasladadas al Museo de Chapultepec.</p>
      <p>.. .las joyas empeñadas tienen un valor de ocho millones de pesos,... la corona es cesárea; está formada por las ramas de laurel en lámina de oro cincelada y unida por estupendo broche que tiene una gran esmeralda orlada de diamantes y rubíes y que sostiene finísimos listones con los colores nacionales.<br>
        Esa corona fue colocada en las sienes del Benemérito en acto público solemne por una gentil señorita; la placa obsequiada por la ciudad de San Francisco, California, es bellísima; de cerca de doce centímetros de alto, en forma oval. En el centro tiene grabada sobre una placa de oro, una composición en que se ve como figura central a Juárez, llevando la bandera tricolor y rodeado por el pueblo; en la parte superior dice &quot;Al Lic. Benito Juárez, y tiene muchas piedras preciosas.<br>
        La medalla peruana fue obsequiada al Benemérito por la escuela de medicina de Lima; las inscripciones dicen: 'Al Doctor don Benito Juárez. Escuela de Medicina de Lima. '<br>
        La medalla otorgada por el Estado de Oaxaca es de oro y piedras preciosas, está en un estuche de marfil preciosamente labrado, entre otras joyas...</p>
      <p>Atrás de cada préstamo existe una historia; gente con necesidades que acude a esta benéfica institución a buscar un alivio.</p>
      <p>Sirvan estas líneas como un sencillo homenaje a todos los que han hecho posible que exista el Monte de Piedad de Oaxaca.</p>
      <p>&nbsp;</p>
      <p>33El Universal domingo 1° de septiembre de 1940. Reportaje de Jacobo Dalevuelta (seudónimo de Martínez de AguiJar), periodista oaxaqueño.</p>

		</div>
    </div>
     -->  
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  
  
  
  
  
  
  
  

</div>
  
@endsection

@include('plantilla.footerPrincipal')
	
   
	</body>

</html>



   