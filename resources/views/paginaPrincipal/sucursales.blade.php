@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')


    <div class="container">

        <div class="row"id="box1">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/sucursales.jpg" /></div>

</div>




	</div>



<nav class="navbar navbar-default suc-menu-1" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="#"></a> </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-main">
               <ul class="nav nav-pills nav-justified"><li><a href="#valles">Valles</a></li>
        <li><a href="#istmo">Istmo</a></li>
        <li><a href="#costa">Costa</a></li>
        <li><a href="#cuenca">Cuenca</a></li>
        <li><a href="#mixteca">Mixteca</a></li>
	  </ul>
            </div>
          </div>
        </nav>

       <div class="container">



<section id="valles" class="padding-bottom-0">  <div class=" col-lg-12"  style="height: 100px; background:#24a093; padding-top: 2%; margin-bottom: 30px; text-align: center"><h2>Valles</h2></div>
  <div class="row">
    <div id="matríz" class="col-lg-12">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/matriz.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Oficina Matríz</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Avenida Morelos 703, Col. Centro, Oaxaca.<br>
							CP. 68000.
							<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:00<br><hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5016267
					  </div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>


  </div>

  <div class="row"><div id="abastos" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/abastos.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Sucursal Abastos</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Valerio Trujano 801, Col. Centro, Oaxaca.<br>
							CP. 68000.
							<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:00<br><hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5165071</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div></div>
<div class="row"> <div id="xoxo" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/xoxo.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Sucursal Xoxo</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							1a. Priv de Progreso 100 local 3, Col. Palestina, Santa Cruz Xoxocotlán, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 14:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5172850</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="20denoviembre" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/20-noviembre.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Sucursal 20 de Noviembre</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							20 de noviembre 707 A, Col. Centro, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5144059</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="modulo-azul" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/moduloazul.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Módulo azul</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Mártires de Chicago esq Proletariado Mexicano<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:30<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5187204</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>

    <div class="row"> <div id="reforma" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/reforma.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Sucursal Reforma</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							H. Colegio Militar 525, Col. Reforma, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 9:00 a 16:30 | Sábados 9:00 a 13:45<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5025219</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="cdjudicial" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/cdjudicial.png" alt=""/></div>
					<div class="col-md-4 col-xs-12">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Cd. Judicial</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Av. Gerardo Pandal Graff, Núcleo de Servicios, Cd Judicial, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5015000 ext 27024</div>
                    </div>
					<div class="col-lg-2 col-xs-3"></div>

    </div>
    </div>
    <div class="row"> <div id="tlacolula" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/tlacolula.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Sucursal Tlacolula</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							2 de abril 20 esq Vicente Guerrero, Tlacolula de Matamoros, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 9:00 a 17:00 | Sábados 9:00 a 14:00 | Domingos 10:00 a 15:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	5620309</div>
                    </div>
					<div class="col-lg-2 col-xs-4"></div>

    </div>
    </div>
</section>

<section id="istmo">
 <div class=" col-lg-12"  style="height: 100px; background:#f5780f; padding-top: 2%; margin-bottom: 30px;margin-top: 30px; text-align: center"><h2>Istmo</h2></div>
 <div class="row">
    <div id="tehuantepec" class="col-lg-12">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/tehuantepec.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Tehuantepec</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Juana C. Romero 18, Barrio San Sebastian, Santo Domingo Tehuantepec, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 14:00<br><hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	971.7150082</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>


  </div>
  <div class="row"> <div id="salinacruz" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/salinacruz.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Salina Cruz</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Manuel Ávila Camacho 415, centro,Salina Cruz, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:30 | Sábados 9:00 a 13:30<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	971.7203026</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="ixtepec" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/ixtepec.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Cd. Ixtepec</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Morelos 34, col Estación,Cd. Ixtepec,Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 14:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	971.7131567</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="juchitan13" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/juchitan13.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Juchitán 13</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Efraín R. Gómez 15 C, col centro, Juchitán, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 17:00 | Sábados 9:00 a 15:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	971.7113289</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="juchitan14" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/juchitan14.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Juchitán 14</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Valentín S. Carrasco 3, col centro, Juchitán, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 17:00 | Sábados 9:00 a 15:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	971.2810862</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
      <div class="row"> <div id="matias" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/matias.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Matías Romero</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							H. Ayuntamiento s/n entre Morelos y 5 de mayo sur, Matías Romero, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 9:00 a 17:00 | Sábados 9:00 a 14:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	972.7220972</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
 <div class="row"> <div id="sanblas" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/sanblas.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>San Blas Atempa</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Francisco Cortés esq Benito Juárez, San Blas Atempa, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	971.715272</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>

</section>

<section id="costa">
<div class=" col-lg-12"  style="height: 100px; background:#4f9815; padding-top: 2%; margin-bottom: 30px;margin-top: 30px; text-align: center"><h2>Costa</h2></div>
<div class="row">
    <div id="puerto-escondido" class="col-lg-12">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/puertoescondido.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Puerto Escondido</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							5a. Norte esq 2a. Poniente, Sector Juárez, Puerto Escondido, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:30<br><hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	954.5821711</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>


  </div>
 <div class="row"> <div id="pochutla" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/sanblas.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>San Pedro Pochutla</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Av. Lázaro Cárdenas Nº128 Esq. Allende, Col. Centro<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 9:00 a 17:00 | Sábados 9:00 a 14:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	958.584037</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
    <div class="row"> <div id="pinotepa" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/pochutla.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Pinotepa Nacional</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Juárez 217 PB, Palacio Municipal, Pinotepa Nacional, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 9:00 a 17:00 | Sábados 9:00 a 14:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	954.5432702</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>
</section>

<section id="cuenca">
<div class=" col-lg-12"  style="height: 100px; background:#b4106b; padding-top: 2%; margin-bottom: 30px;margin-top: 30px; text-align: center">
  <h2>Cuenca del Papaloapán</h2></div>
<div class="row">
    <div id="tuxtepec" class="col-lg-12">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/tuxtepec.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Tuxtepec</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							20 de noviembre esq. Guerrero, Tuxtepec, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:30<br><hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	287.8754055<br>
	287.8754235</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>


  </div>
 <div class="row"> <div id="lomabonita" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/loma.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Loma Bonita</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Michoacán 25, loc 1 y 2, Loma Bonita, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 17:00 | Sábados 9:00 a 14:00<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	281.8720003</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>

</section>

<section id="mixteca">
<div class=" col-lg-12"  style="height: 100px; background:#568feb; padding-top: 2%; margin-bottom: 30px;margin-top: 30px; text-align: center">
  <h2>Mixteca</h2></div>
<div class="row">
    <div id="huajuapan" class="col-lg-12">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/huajuapan.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Huajuapan</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Cuauhtemoc 16, Centro, Heroica Ciudad de Huajuapan de León, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:30<br><hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	953.5321624</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>


  </div>
 <div class="row"> <div id="tlaxiaco" class="col-lg-12 borde-arriba">
					<div class="col-md-6"><img class="img-responsive img-pad" src="img/sucursales/tlaxiaco.png" alt=""/></div>
					<div class="col-md-4">

					  <p class="product-description"><strong><span class="glyphicon glyphicon-home" ></span>Tlaxiaco</strong></p>

                      <div class="direcciones"><hr align="left"><strong><span class="glyphicon glyphicon-map-marker" ></span>Dirección:</strong><br>
							Aldama 6, centro, Tlaxiaco, Oaxaca.<br>
							<hr align="left">
					    <strong>Horario:</strong><br>
							Lunes a Viernes 8:30 a 16:00 | Sábados 9:00 a 13:30<br>
							<hr align="left">
<strong><span class="glyphicon glyphicon-phone-alt" ></span>Teléfono:</strong><br>
	953.5521173</div>
                    </div>
					<div class="col-lg-2 col-xs-6"></div>

    </div>
    </div>

</section>






<ul class="nav pull-right scroll-top">
  <li><a href="#section1" title="Scroll to top"><i class="glyphicon glyphicon-chevron-up"></i></a></li>
</ul>

		</div>


@endsection

@include('plantilla.footerPrincipal')



       <script>

	/* activate scrollspy menu */
$('body').scrollspy({
  target: '#navbar-collapsible',
  offset: 50
});

/* animate smooth scrolling sections */
$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 50
        }, 1000);
        return false;
      }
    }
});

	$(document).ready(function(){
    $(window).bind('scroll', function() {
        var navHeight = $("#box1").height();
        ($(window).scrollTop() > navHeight) ? $('nav').addClass('goToTop') : $('nav').removeClass('goToTop');
    });
});
</script>
