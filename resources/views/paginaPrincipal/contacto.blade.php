@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')


    <div class="container">

        <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/contacto.jpg" /></div>

</div>
      <div class="row">
		  <div class="col-lg-6 col-md-6 formulario-con">
		    <h2 class="contacto"><span class="glyphicon glyphicon-pencil"></span> Escríbenos</h2><div class="col-lg-10 col-lg-offset-1 articulos-empe text-center"><h2>Buzón de quejas y sugerencias.</h2><br><br>contacto@montedepiedad.gob.mx</p>  <br><br><br><br><br><br>



  </div>

  </form>
</div>
  <div class="col-lg-6 col-md-6 formulario-con">
      <h2 class="contacto"><span class="glyphicon glyphicon-phone-alt"></span> Llámanos</h2><div class="col-lg-10 col-lg-offset-1 articulos-empe"><h2>Contacta a tu sucursal más cercana</h2><h4>Oficina Matriz<br>
Avenida Morelos 703,<br>
C.P. 68000, Centro,<br>
Oaxaca de Juárez, Oaxaca.<br>

Tel. 01-951-51 6 27 56<br><br>
  <a class="articulos-boton"href="{{url('/sucursales')}}">Más sucursales</a>
  </div></div>
      </div>
    </div>
@endsection

@include('plantilla.footerPrincipal')
