@extends('main')
@include('plantilla.headerPrincipal')

@section('contenido')


       
       <div class="container">

       <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/articulos.jpg" /></div>
        </div>
          <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" style="height: 100px;background:#F7AE3C;padding-top: 20px; ">
    <h2 class="text-center">Desde un anillo hasta tu auto pueden ayudarte a cumplir tus metas.</h2>
    
  </div>
       </div>
        <div class="row">
          <div class="col-lg-9 arti">
            <div class="row">
              <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/joyeria.png"/></div>
              <div class="col-lg-4">
                <h3>Joyas</h3>
                    
                <p>Únicamente fabricadas en Oro y Plata.</p>
              </div>
              <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/reloj.png"/></div>
<div class="col-lg-4">
          <h3>Relojes</h3>
                <p>Únicamente con pulso de oro.</p>
              </div>
            </div>
            <div class="row" style="margin-top: 40px">
            <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/carro.png"/></div>
<div class="col-lg-8">
  <h3>Autos</h3>
    <p>Estos son algunos de los requisitos que tu auto debe cumplir:</p>
    <ul>
      <li>Deben ser modelos con máximo 8 años de antigüedad.</li>
      <li>Todos los papeles en regla.</li>
      <li>Debes contar con la factura original.</li>
      </ul>
</div>
            </div>
                <div></div>
                <div class="row"  style="margin-top: 40px">
                 <div class="col-lg-2"><img class="img-responsive quempenar" src="img/iconos/varios.png"/></div>
<div class="col-lg-8">
  <h3>Varios                </h3>
                    <ul>
                      <li>Electrónica excepto celulares.</li>
                      <li>Baterías de Cocina.</li>
                      <li>Bicicletas.</li>
                      <li>Herramientas y equipo de construcción.</li>
                      <li>Electrodomésticos.</li>
                      <li>Equipo de cómputo todo en uno y laptops.</li>
                      <li>Consolas de video.</li>
                      
                      <li>Instrumentos Musicales.</li>
                      <li>Línea Blanca.</li>
                      <li>Máquinas de Coser.</li>
                      <li>Vajillas.</li>
                      <li>Entre otros muchos artículos.                <br>
                        <br>
                      </li>
                      </ul>
                </div>
                </div>
          </div>
          <div class="col-lg-3 col-offset-lg-1"><div class="col-lg-12 articulos-empe text-center"><h2>Movimientos<br>
a tu boleta</h2>
  <a class="articulos-boton"href="{{url('/pagos')}}">Abono</a><br>
    <a class="articulos-boton"href="{{url('/pagos')}}">Refrendo</a><br>
      <a class="articulos-boton"href="{{url('/pagos')}}">Demasía</a><br>
      <a class="articulos-boton"href="{{url('/pagos')}}">Desempeño</a>


  </div><br>
<br>
<br>
<h3 class="advertencia">Esta lista es de carácter informativo e ilustrativo, acércate a tu sucursal más cercana para recibir orientación. Recuerda que los artículos deben de funcionar correctamente y venir con todos sus accesorios.</h3>
</div>
        </div>
    </div>
  @endsection

@include('plantilla.footerPrincipal')

    		</body>

</html>		