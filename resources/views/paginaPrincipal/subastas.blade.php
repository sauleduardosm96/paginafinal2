@extends ('main')
@include('plantilla.headerPrincipal')

@section('contenido')

  <div class="container">

       <div class="row">
          <div class="col-lg-12"><img class="img-responsive" src="img/banners/subastas.jpg" /></div>
          <div class="col-lg-12 text-center" style="background: #640C46"><h2>Calendario de subastas<br>
          <div id="mesSubasta_id">2019</div></h2></div>
        </div>
      <div class="col-lg-4">
          <p class="texto-empenos1"><span class="resaltado">Las prendas que pasaron a Almoneda y a las que no se hizo algún movimiento se subastan públicamente en cada sucursal.</span> </p>

          <div class="col-lg-12" style="text-align: center"><img class="img-responsive quempenar" src="img/iconos/publico.png"/>
          </div>
          <div class="col-lg-12" style="padding: 0 30px">

              <p class="derecha-empeños"> Las subastas se realizan cada 15 días en las sucursales de acuerdo al calendario de subastas puede participar en ella cualquier persona.</p>

          </div>
          <div class="col-lg-12" style="text-align: center"><img class="img-responsive quempenar" src="img/iconos/subasta.png"/>
          </div>
          <div class="col-lg-12"  style="padding: 0 30px">
              <p class="derecha-empeños"> Las prendas se ofrecen al precio de avalúo y, en caso de que haya 2 o más compradores interesados en la misma, se realizan pujas de 100 pesos hasta llegar al mejor postor quien se lleva la prenda.</p>
          </div>

      </div>






          <div class="col-lg-8" style="margin-top: 30px">
            <table class="table table-striped subastas" width="100%" border="0" cellspacing="0" cellpadding="10" id= "tSubastas_id">

              <tbody>

                <!--
                        <tr>
                          <td width="70%">
                            <p>
                            <strong>OFICINA MATRIZ</strong>
                                 <br>AV. MORELOS 703 CENTRO,
                                 <br>
                                  OAXACA DE JUAREZ, OAXACA                                     
                            </p>
                                <strong>DE 9:30 A 14:30  HRS.</strong>
                          </td>
                          <td width="30%">
                            <strong>VARIOS 10 -24<br>ALHAJAS 14 – 15-24 y 27</strong>
                          </td>
                        </tr>

                        <tr>
                    <td><p><strong>TEHUANTEPEC</strong> <br>
                      AV. JUANA C. ROMERO NUM.18 <br>
                      BARRIO SAN SEBASTIAN                                                </p>
                      <strong>DE 10:00 A 14:00 HRS.</strong></td>
                    <td><strong>VARIOS    7</strong>
                      <strong><br>
                      ALHAJAS   21</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>ABASTOS   </strong>                          <br>
                      CALLE VALERIO TRUJANO NUM. 801 <br>
                      OAXACA DE JUÁREZ, OAXACA                              </p>
                      <strong>DE 10:30 A 14:00 HRS.</strong></td>
                    <td><strong>VARIOS               9 y 23              ALHAJAS             7 y 21</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>HUAJUAPAN</strong> <br>
                      CALLE CUAUHTEMOC NUM. 16      </p>
                      <strong>DE 10:00 A 12:00 HRS</strong></td>
                    <td><strong>VARIOS      8 y 22</strong>
                      <strong><br>
                      ALHAJAS     8 y 22</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>TUXTEPEC</strong> <br>
                      AV. 20 DE NOVIEMBRE NUM.170                                                            </p>
               <strong>DE 10:00 A 13:00HRS</strong></td>
                    <td><strong>VARIOS        7 y 21<br>
                      ALHAJAS      9 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>PUERTO ESCONDIDO</strong> <br>
                      CALLE 5a NORTE ESQ. 2a PONIENTE  <br>
                      SECTOR JUAREZ</p>
                      <strong>DE 10:00 A 16:00  HRS.</strong></td>
                    <td><strong>VARIOS  8 y 21<br>
                      ALHAJAS 10 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>SALINA CRUZ</strong> <br>
                      AV. MANUEL AVILA CAMACHO 415 <strong>                                              </strong></p>
                      <strong>DE 10:00 A 15:00 HRS.</strong></td>
                    <td><strong>VARIOS  Y ALHAJAS <br>
                      3 y 20</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>XOXOCOTLAN</strong> <br>
              1a PRIV. DE PROGRESO NUM. 100                                            <br>
              SANTA CRUZ XOXOCOTLAN </p>
                      <strong>DE 10:00 A 14:00 HRS.</strong>
                      <p>                                        </p></td>
                    <td><strong>VARIOS Y ALHAJAS <br>
                      6 y 20</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>20 DE NOVIEMBRE</strong> <br>
                      CALLE 20 DE NOVIEMBRE NUM. 707-B <br>
                      CENTRO</p>
                      <strong>DE 10:00 A 14:00 HRS</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      3 y 17</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>LOMA BONITA</strong> <br>
                      CALLE MICHOACAN NUM. 25, LOCAL 3                                                             </p>
               <strong>DE 10:00 A 12:00HRS</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      8 y 22</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>PINOTEPA</strong><br>
                        AV. JUAREZ 217 CENTRO  <strong>                                                           </strong></p>
                      <strong>DE 10:00 A 12:00HRS</strong></td>
                    <td><strong>VARIOS                9 y 23<br>
                      ALHAJAS             7 y 21</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>IXTEPEC</strong><br>
                      CALLE MORELOS 34<br>
                      COL. ESTACION</p>
                      <strong>DE 9:00 A 11:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      7 y 21</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>MODULO AZUL</strong><br>
                      BOULEVARD MARTIRES DE CHICAGO Y AV.PROL. MEXICANO FOVISSSTE <br>
                      OAXACA DE JUAREZ,OAX.     <strong>                                         </strong></p>
                      <strong> DE 9:00 A 11:00 HRS.</strong></td>
                    <td><strong>VARIOS                7 y 21<br>
                      ALHAJAS             9 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>JUCHITAN SUC. 13</strong><br>
                      CALLE EFRAIN R. GOMEZ 15 C    <strong>                            </strong></p>
                      <strong>DE 9:00 A 11:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      3 y 20</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>JUCHITAN SUC. 14</strong><br>
                      CALLE VALENTIN S. CARRASCO NUM. 3 <strong>                                            </strong></p>
                      <strong>DE 10:00 A 14:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      8 y 22</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>MATÍAS ROMERO</strong><br>
                      AYUNTAMIENTO S/N, ENTRE MORELOS Y <br>
                      5 DE MAYO SUR</p>
                      <strong>DE 10:00 A 14:00HRS</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      3 y 20</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>REFORMA</strong><br>
                      HEROICO COLEGIO MILITAR 525, <br>
                      COL. REFORMA <strong>                    </strong></p>
                      <strong>DE 10:00 A 14:00 HRS.</strong></td>
                    <td><strong>AUTOMOVILES Y MOTOCICLETAS   7 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>SAN BLAS ATEMPA</strong><br>
                        FRANCISCO CORTEZ S/N COL. CENTRO       <strong>                                       </strong></p>
                      <strong> DE 10:00 A 14:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS <br>
                      9 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>POCHUTLA</strong><br>
                      AV. LAZARO CARDENAS NUM. 17, ESQ. CALLE LAS PALMAS</p>
                      <strong>DE 10:00 A 13:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      7 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>CD. JUDICIAL</strong><br>
                      AV. GERARDO PANDAL GRAFF 1, FRACC. REYES MANTECON,<br>
                      SAN BARTOLO COYOTEPEC   <strong>                                          </strong></p>
                      <strong>DE 10:00 A 13:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
              8 y 22</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>TLAXIACO</strong><br>
                      CALLE ALDAMA NUM. 6 ENTRE LAS CALLES INDEPENDENCIA E HIDALGO                <strong>                                                       </strong></p>
                      <strong>DE 10:00 A 13:00HRS</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      8 y 23</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>TLACOLULA</strong><br>
                      CALLE 2 DE ABRIL NUMERO 20  ESQ. CALLE VICENTE GUERRERO             <strong>                                </strong></p>
                      <strong>DE 10:00 A 13:00 HRS.</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      2 y 16</strong></td>
                  </tr>
                  <tr>
                    <td><p><strong>MIAHUATLAN</strong><br>
                      CALLE BASILIO ROJAS ESQUINA ORIENTE  <strong>                                                                     </strong></p>
                      <strong>DE 10:00 A 13:00HRS</strong></td>
                    <td><strong>VARIOS Y ALHAJAS<br>
                      8 y 22</strong></td>
                  </tr>
                 -->
@if(isset($subs))
                 @foreach ($subs as $s)
                   <tr>

                              <td width="70%">
                                <p>
                                <strong>{{$s->sucursal}}</strong>
                                     <br> {{utf8_decode($s->direccion)}}

                                </p>
                                    <strong>DE 9:30 A 14:30&nbsp; HRS.</strong>
                              </td>
                              <td width="30%">
                                @if ($s->c_tipo_subasta==1)
                                    <strong><br>ALHAJAS   {{date("d/m/Y", strtotime($s->fecha_subasta))}}</strong>
                                @else
                                    <strong><br>VARIOS   {{date("d/m/Y", strtotime($s->fecha_subasta))}}</strong>
                                @endif

                              </td>
                    </tr>
                 @endforeach
@endif
               </tbody>
 </table>

</div>

    </div>


@endsection

@include('plantilla.footerPrincipal')
