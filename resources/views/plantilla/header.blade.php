	<!DOCTYPE html>
<html lang="es">

<head>


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Monte de Piedad del Estado de Oaxaca</title>
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon"/>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/business-casual.css') }}" rel="stylesheet">



    <link href="https://fonts.googleapis.com/css?family=Biryani:400,700" rel="stylesheet">


</head>

<body>

    <div class="brand">
      <div class="container">
               <div class="row flex-parent">
	<div class="col-xs-2">
		<a href="http://montedepiedad.gob.mx/"><img src=" {{ asset('img/iconos/monte-de-piedad-logo.png') }}" class="img-responsive center-block" /></a>
	</div>

	<div class="col-xs-1 text-right">
		<p class="texto-header">Horario de Servicio</p></div>
		<div class="col-xs-1 text-center">
		<img src="{{ asset('img/iconos/horario.png')}}" width="58" height="58" alt=""/> </div>
		<div class="col-xs-2 flex-child text-left"><p class="texto-header">8:30 a 16:00 h | Lunes a Viernes <br>
Sábados | 9:00 a 13:00 h
<br>
		</p>
		 </div>

		<div class="col-xs-1 flex-child text-right"><p class="texto-header">Preguntas<br>
Frecuentes </p>
		</div>
		<div class="col-xs-1 text-center">
		<a href=""><img src="{{ asset('img/iconos/faqs-header.png') }}" width="58" height="58" alt=""/></a> </div>
		<div class="col-xs-1 flex-child text-center">

<ul class="nav nav-pills" style="margin-top: 10px">
  <li><a href=" " style="color: #B4106B">Contacto</a></li>
</ul>


		</div><div class="col-xs-1">
		</div><div class="col-xs-1 flex-child text-center">
		<a href="https://twitter.com/mpiedad_goboax?lang=es" target="_blank"><img src="{{ asset('img/iconos/twitter.png') }}" width="58" height="58" alt=""/></a> </div><div class="col-xs-1 flex-child text-center">
		<a href="https://www.facebook.com/Monte-de-Piedad-del-Estado-de-Oaxaca-170349739685921/" target="_blank"><img src="{{ asset('img/iconos/facebook.png') }}" width="58" height="58" alt=""/></a> </div>
</div>



          </div>


</div>

     <nav class="navbar navbar-default" role="navigation">
        <div class="container">


            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"> <img class="img-responsive" src="{{ asset('img/iconos/monte-de-piedad-logo.png') }}"  alt=""/></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li>
                        <a href="http://montedepiedad.gob.mx/">Página Web</a>
                    </li>
                   <li class="dropdown">
                   <li>
                       <a href="{{ url('/') }}">Inicio </a>
                    </li>
                    <li>
                      <a href="{{ url ('/transparencia07') }}">Transparencia hasta 2017</a>
                    </li>
                    <li>
                      <a href="{{ url ('/transparencia') }}">Transparencia a partir del 2018</a>
                    </li>
                    <li>
                        <a href= "{{ URL('fiscal') }}">Plataforma Financiera</a>
                    </li>
                    <li>
                        <a href= "{{ route('contable.index') }}">Contable</a>
                    </li>
                    <li>
                        <a href= "{{ route('presupuestal.index') }}">Presupuestal</a>
                    </li>
                     <li>
                        <a href= "{{ URL('disciplinaFinanciera') }}">Disciplina Financiera</a>
                    </li>
                    <li>
                        <a href= "{{ route('financieraContable.index') }}">Disciplina Contable</a>
                    </li>
                    <li>
                        <a href= "{{ route('financieraPresupuestal.index') }}">Disciplina Presupuestal</a>
                    </li>




     </div>
            </li>


     </div>


                  </ul>
            </div>


        </div>

    </nav>
